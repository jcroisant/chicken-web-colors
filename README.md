# web-colors

Parse and write HTML/CSS color strings in CHICKEN Scheme.

- **Version:**      1.0.0 (2019-12-31)
- **Project:**      https://gitlab.com/jcroisant/chicken-web-colors
- **Issues:**       https://gitlab.com/jcroisant/chicken-web-colors/issues
- **License:**      [BSD](LICENSE-BSD.txt)
- **Docs:**         https://wiki.call-cc.org/eggref/5/web-colors
- **Maintainer:**   John Croisant (john (at) croisant (dot) net)


## Synopsis

This library can parse and write colors in a variety of
[formats used in HTML/CSS](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value):

- Hex colors in `#RGB`, `#RGBA`, `#RRGGBB`, and `#RRGGBBAA` formats
- `rgb()` and `rgba()` colors
- `hsl()` and `hsla()` colors
- Named colors, a.k.a. color keywords

This library does not provide color math or color space conversion.
It is intended to be used with other libraries.


## Installation and usage

Installation: `chicken-install web-colors`

Usage:

```scheme
(import web-colors)

(parse-web-color "#B64926") ; → (rgb 182 73 38 1)
(web-color->string '(rgb 182 73 38 1)) ; → "#b64926"

(parse-web-color "rgb(93 152 121 / 51%)") ; → (rgb 93 152 121 51/100)
(web-color->string '(rgb 93 152 121 51/100)) ; → "rgba(93, 152, 121, 0.51)"

(parse-web-color "hsl(210 51% 87% / 77%)") ; → (hsl 210 51/100 87/100 77/100)
(web-color->string '(hsl 210 51/100 87/100 77/100)) ; → "hsla(210, 51%, 87%, 0.77)"

(parse-web-color "indigo") ; → (rgb 75 0 130 1)
(web-color->string '(rgb 75 0 130 1)) ; → "#4b0082"
(web-color-name '(rgb 75 0 130 1)) ; → "indigo"
```

See the [docs](https://wiki.call-cc.org/eggref/5/web-colors)
for more examples and information.


## Code of Conduct

Please be aware that all project participants are expected to abide by
the [Code of Conduct](CODE_OF_CONDUCT.md).
We are committed to making participation in this project a welcoming
and harassment-free experience.
