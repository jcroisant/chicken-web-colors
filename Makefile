default: install-test

install:
	chicken-install -s

install-test:
	chicken-install -s -test

uninstall:
	chicken-uninstall web-colors

test:
	cd tests && csi -s run.scm

clean:
	rm -f *.import.scm *.types *.c *.o *.so *.link *.build.sh *.install.sh
