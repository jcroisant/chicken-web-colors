;;
;; chicken-web-colors:
;; Parse and write HTML/CSS color strings in CHICKEN Scheme.
;;
;; Copyright © 2019  John Croisant.
;; All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions
;; are met:
;;
;; - Redistributions of source code must retain the above copyright
;;   notice, this list of conditions and the following disclaimer.
;;
;; - Redistributions in binary form must reproduce the above copyright
;;   notice, this list of conditions and the following disclaimer in
;;   the documentation and/or other materials provided with the
;;   distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
;; FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
;; COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
;; INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
;; (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
;; SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
;; HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
;; STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
;; OF THE POSSIBILITY OF SUCH DAMAGE.


(export *web-color-names*
        web-color-name)


(define *web-color-names*
  (make-parameter
   `((aliceblue . (rgb 240 248 255 1))
     (antiquewhite . (rgb 250 235 215 1))
     (aqua . (rgb 0 255 255 1))
     (aquamarine . (rgb 127 255 212 1))
     (azure . (rgb 240 255 255 1))
     (beige . (rgb 245 245 220 1))
     (bisque . (rgb 255 228 196 1))
     (black . (rgb 0 0 0 1))
     (blanchedalmond . (rgb 255 235 205 1))
     (blue . (rgb 0 0 255 1))
     (blueviolet . (rgb 138 43 226 1))
     (brown . (rgb 165 42 42 1))
     (burlywood . (rgb 222 184 135 1))
     (cadetblue . (rgb 95 158 160 1))
     (chartreuse . (rgb 127 255 0 1))
     (chocolate . (rgb 210 105 30 1))
     (coral . (rgb 255 127 80 1))
     (cornflowerblue . (rgb 100 149 237 1))
     (cornsilk . (rgb 255 248 220 1))
     (crimson . (rgb 220 20 60 1))
     (cyan . (rgb 0 255 255 1))
     (darkblue . (rgb 0 0 139 1))
     (darkcyan . (rgb 0 139 139 1))
     (darkgoldenrod . (rgb 184 134 11 1))
     (darkgray . (rgb 169 169 169 1))
     (darkgreen . (rgb 0 100 0 1))
     (darkgrey . (rgb 169 169 169 1))
     (darkkhaki . (rgb 189 183 107 1))
     (darkmagenta . (rgb 139 0 139 1))
     (darkolivegreen . (rgb 85 107 47 1))
     (darkorange . (rgb 255 140 0 1))
     (darkorchid . (rgb 153 50 204 1))
     (darkred . (rgb 139 0 0 1))
     (darksalmon . (rgb 233 150 122 1))
     (darkseagreen . (rgb 143 188 143 1))
     (darkslateblue . (rgb 72 61 139 1))
     (darkslategray . (rgb 47 79 79 1))
     (darkslategrey . (rgb 47 79 79 1))
     (darkturquoise . (rgb 0 206 209 1))
     (darkviolet . (rgb 148 0 211 1))
     (deeppink . (rgb 255 20 147 1))
     (deepskyblue . (rgb 0 191 255 1))
     (dimgray . (rgb 105 105 105 1))
     (dimgrey . (rgb 105 105 105 1))
     (dodgerblue . (rgb 30 144 255 1))
     (firebrick . (rgb 178 34 34 1))
     (floralwhite . (rgb 255 250 240 1))
     (forestgreen . (rgb 34 139 34 1))
     (fuchsia . (rgb 255 0 255 1))
     (gainsboro . (rgb 220 220 220 1))
     (ghostwhite . (rgb 248 248 255 1))
     (gold . (rgb 255 215 0 1))
     (goldenrod . (rgb 218 165 32 1))
     (gray . (rgb 128 128 128 1))
     (green . (rgb 0 128 0 1))
     (greenyellow . (rgb 173 255 47 1))
     (grey . (rgb 128 128 128 1))
     (honeydew . (rgb 240 255 240 1))
     (hotpink . (rgb 255 105 180 1))
     (indianred . (rgb 205 92 92 1))
     (indigo . (rgb 75 0 130 1))
     (ivory . (rgb 255 255 240 1))
     (khaki . (rgb 240 230 140 1))
     (lavender . (rgb 230 230 250 1))
     (lavenderblush . (rgb 255 240 245 1))
     (lawngreen . (rgb 124 252 0 1))
     (lemonchiffon . (rgb 255 250 205 1))
     (lightblue . (rgb 173 216 230 1))
     (lightcoral . (rgb 240 128 128 1))
     (lightcyan . (rgb 224 255 255 1))
     (lightgoldenrodyellow . (rgb 250 250 210 1))
     (lightgray . (rgb 211 211 211 1))
     (lightgreen . (rgb 144 238 144 1))
     (lightgrey . (rgb 211 211 211 1))
     (lightpink . (rgb 255 182 193 1))
     (lightsalmon . (rgb 255 160 122 1))
     (lightseagreen . (rgb 32 178 170 1))
     (lightskyblue . (rgb 135 206 250 1))
     (lightslategray . (rgb 119 136 153 1))
     (lightslategrey . (rgb 119 136 153 1))
     (lightsteelblue . (rgb 176 196 222 1))
     (lightyellow . (rgb 255 255 224 1))
     (lime . (rgb 0 255 0 1))
     (limegreen . (rgb 50 205 50 1))
     (linen . (rgb 250 240 230 1))
     (magenta . (rgb 255 0 255 1))
     (maroon . (rgb 128 0 0 1))
     (mediumaquamarine . (rgb 102 205 170 1))
     (mediumblue . (rgb 0 0 205 1))
     (mediumorchid . (rgb 186 85 211 1))
     (mediumpurple . (rgb 147 112 219 1))
     (mediumseagreen . (rgb 60 179 113 1))
     (mediumslateblue . (rgb 123 104 238 1))
     (mediumspringgreen . (rgb 0 250 154 1))
     (mediumturquoise . (rgb 72 209 204 1))
     (mediumvioletred . (rgb 199 21 133 1))
     (midnightblue . (rgb 25 25 112 1))
     (mintcream . (rgb 245 255 250 1))
     (mistyrose . (rgb 255 228 225 1))
     (moccasin . (rgb 255 228 181 1))
     (navajowhite . (rgb 255 222 173 1))
     (navy . (rgb 0 0 128 1))
     (oldlace . (rgb 253 245 230 1))
     (olive . (rgb 128 128 0 1))
     (olivedrab . (rgb 107 142 35 1))
     (orange . (rgb 255 165 0 1))
     (orangered . (rgb 255 69 0 1))
     (orchid . (rgb 218 112 214 1))
     (palegoldenrod . (rgb 238 232 170 1))
     (palegreen . (rgb 152 251 152 1))
     (paleturquoise . (rgb 175 238 238 1))
     (palevioletred . (rgb 219 112 147 1))
     (papayawhip . (rgb 255 239 213 1))
     (peachpuff . (rgb 255 218 185 1))
     (peru . (rgb 205 133 63 1))
     (pink . (rgb 255 192 203 1))
     (plum . (rgb 221 160 221 1))
     (powderblue . (rgb 176 224 230 1))
     (purple . (rgb 128 0 128 1))
     (rebeccapurple . (rgb 102 51 153 1))
     (red . (rgb 255 0 0 1))
     (rosybrown . (rgb 188 143 143 1))
     (royalblue . (rgb 65 105 225 1))
     (saddlebrown . (rgb 139 69 19 1))
     (salmon . (rgb 250 128 114 1))
     (sandybrown . (rgb 244 164 96 1))
     (seagreen . (rgb 46 139 87 1))
     (seashell . (rgb 255 245 238 1))
     (sienna . (rgb 160 82 45 1))
     (silver . (rgb 192 192 192 1))
     (skyblue . (rgb 135 206 235 1))
     (slateblue . (rgb 106 90 205 1))
     (slategray . (rgb 112 128 144 1))
     (slategrey . (rgb 112 128 144 1))
     (snow . (rgb 255 250 250 1))
     (springgreen . (rgb 0 255 127 1))
     (steelblue . (rgb 70 130 180 1))
     (tan . (rgb 210 180 140 1))
     (teal . (rgb 0 128 128 1))
     (thistle . (rgb 216 191 216 1))
     (tomato . (rgb 255 99 71 1))
     (transparent . (rgb 0 0 0 0))
     (turquoise . (rgb 64 224 208 1))
     (violet . (rgb 238 130 238 1))
     (wheat . (rgb 245 222 179 1))
     (white . (rgb 255 255 255 1))
     (whitesmoke . (rgb 245 245 245 1))
     (yellow . (rgb 255 255 0 1))
     (yellowgreen . (rgb 154 205 50 1)))))


(: web-color-name (color-list --> (or string false)))
(define (web-color-name color)
  (let look ((names (*web-color-names*)))
    (cond ((null? names)
           #f)
          ((equal? (cdar names) color)
           (symbol->string (caar names)))
          (else
           (look (cdr names))))))
